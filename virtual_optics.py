'''
  virtual_bench: a simple tool for visualizing 1D optics with lenses
  Uses a purely ray-optics approach.

  Copyright 2018 Carter Turnbaugh

  virtual_bench is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  virtual_bench is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with virtual_bench.  If not, see <http://www.gnu.org/licenses/>.
'''

from math import cos, pi, sin, sqrt

class LightBeam:

    class Ray:
        def __init__(self, origin, y, t): # t is theta, angle from horizontal
            self.origin = origin
            self.y = y
            self.t = t

        def mat_mult(self, matrix):
            return LightBeam.Ray(self.origin, matrix[0][0]*self.y + matrix[0][1]*self.t,
                                 matrix[1][0]*self.y + matrix[1][1]*self.t)

        def propogate(self, position):
            return LightBeam.Ray(position, self.y + (position - self.origin) * self.t, self.t)
                                 

    class Polarization:
        def __init__(self, u, v):
            self.u = u
            self.v = v

        def mat_mult(self, matrix):
            return LightBeam.Polarization(matrix[0][0]*self.u +  matrix[0][1]*self.v,
                                          matrix[1][0]*self.u +  matrix[1][1]*self.v)

    def __init__(self, ray, polarization):
        self.ray = ray
        self.polarization = polarization

    def at(self, position):
        return self.ray.propogate(position).y

    def propogate(self, position, ray_matrix=[[1,0], [0,1]], polarization_matrix=[[1,0], [0,1]]):
        ray = self.ray.propogate(position).mat_mult(ray_matrix)
        polarization = self.polarization.mat_mult(polarization_matrix)

        return LightBeam(ray, polarization)

class BenchItem:

    def __init__(self, position):
        self.position = position

    def __str__(self):
        return 'Bench Item at ' + str(self.position)

    def __lt__(self, other):
        # Define less than so that we can sort by position to propogate images
        return self.position < other.position

class OpticalItem(BenchItem):

    def __init__(self, position):
        super(OpticalItem, self).__init__(position)
        self.ray_matrix = [[1, 0], [0, 1]]
        self.polarization_matrix = [[1, 0], [0, 1]]

    def update(self, input_beams):
        return [ib.propogate(self.position, self.matrix, self.polarization_matrix)
                for ib in input_beams]

class ThinLens(OpticalItem):

    def __init__(self, focal_length, position):
        super(ThinLens, self).__init__(position)
        self.focal_length = focal_length
        self.matrix = [[1, 0], [-1/self.focal_length, 1]]
        self.polarization_matrix = [[1, 0], [0, 1]]

    def __str__(self):
        return 'Thin Lens at ' + str(self.position) + ' with Focal Length ' + str(self.focal_length)

class LinearPolarizer(OpticalItem):

    def __init__(self, angle, position):
        super(LinearPolarizer, self).__init__(position)
        self.angle = angle
        self.matrix = [[1, 0], [0, 1]]
        # We will do this by rotating the beam,
        # sending it through a horizontal polarizer,
        # then rotating it back
        self.polarization_matrix = [[cos(angle*pi/180)**2, cos(angle*pi/180)*sin(angle*pi/180)],
                                    [cos(angle*pi/180)*sin(angle*pi/180), sin(angle*pi/180)**2]]
        print(self.polarization_matrix)

    def __str__(self):
        return 'Linear Polarizer at ' + str(self.position) + ' at Angle ' + str(self.angle)

class QuarterWavePlate(OpticalItem):

    def __init__(self, angle, position):
        super(QuarterWavePlate, self).__init__(position)
        self.angle = angle
        self.matrix = [[1, 0], [0, 1]]
        # We will do this by rotating the beam,
        # sending it through a horizontal polarizer,
        # then rotating it back
        c = cos(angle*pi/180)
        s = sin(angle*pi/180)
        scale = sqrt(2)/2 + 1j*sqrt(2)/2
        self.polarization_matrix = [[scale*(c**2 + 1j*s**2), scale*(c*s - 1j*c*s)],
                                    [scale*(c*s - 1j*c*s), scale*(s**2 + 1j*c**2)]]
        print(self.polarization_matrix)

    def __str__(self):
        return 'Quarter Wave Plate at ' + str(self.position) + ' at Angle ' + str(self.angle)

class Screen(BenchItem):

    def __str__(self):
        return 'Screen at ' + str(self.position)

    def update(self, input_beams):
        return []

class Object(BenchItem):
    def __init__(self, height, position):
        super(Object, self).__init__(position)
        self.height = height
        self.beam_count = 5

    def update(self):
        beams = []
        for i in range(0, self.beam_count):
            ray = LightBeam.Ray(self.position, self.height,
                                (float(i)+0.5) / 4 - float(self.beam_count) / 8)
            polarization = LightBeam.Polarization(1, 0)
            beams.append(LightBeam(ray, polarization))
        return beams

class BeamSet:
    def __init__(self, beams, position, end_position = None):
        self.beams = beams
        self.position = position
        if end_position == None:
            end_position = self.position
        self.end_position = end_position
    
def generate_beams(source_object, bench_items):
    '''
    Propogate beams from the source_object through the bench_items
    Returns a list of BeamSets
    '''
    beams = [BeamSet(source_object.update(), source_object.position)]

    bench_items.sort()
    for bench_item in bench_items:
        position = bench_item.position
        beams[-1].end_position = position
        beams.append(BeamSet(bench_item.update(beams[-1].beams), position))

    return beams

def get_intersection(beam_set):
    '''
    Gets the intersection of a set of beams by minimizing the distance between two central beams
    by iteratively approaching to within +/- 0.0001
    '''
    if len(beam_set.beams) < 2:
        return None

    # Get the central beams
    beams = sorted(beam_set.beams, key=lambda x: abs(x.ray.t))[0:2]

    intersect = beams[0].ray.origin
    step = 100
    if beams[0].ray.t > beams[1].ray.t:
        step = -step
    if abs(beams[0].ray.t - beams[1].ray.t) < 0.000001:
        # parallel
        return None
    while abs(step) > 0.0001:
        while beams[0].at(intersect) > beams[1].at(intersect):
            intersect += step
        intersect -= step
        while beams[0].at(intersect) < beams[1].at(intersect):
            intersect -= step
        intersect += step
        step = step / 10

    return round(intersect, 4)

if __name__ == "__main__":
    bench_items = [ThinLens(300, 310), ThinLens(300, 610), Screen(910)]
    source_object = Object(200, 10)
