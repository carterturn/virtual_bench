'''
  virtual_bench: a simple tool for visualizing 1D optics with lenses
  Uses a purely ray-optics approach.

  Copyright 2018 Carter Turnbaugh

  virtual_bench is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  virtual_bench is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with virtual_bench.  If not, see <http://www.gnu.org/licenses/>.
'''

from functools import partial
from inspect import signature
from math import sqrt
from tkinter import (Button, Canvas, Entry, Frame, HORIZONTAL, Label,
                     LabelFrame, Menu, Scale, TclError, Tk, Toplevel)

from virtual_optics import *

def get_pointer_canvas_xy(s_canvas):
    return (window.winfo_pointerx() - window.winfo_rootx(),
            canvas['h']/2 - (window.winfo_pointery() - window.winfo_rooty()))

def draw_beams(beams, s_canvas):
    s_canvas['c'].delete('item')
    s_canvas['c'].delete('beam')

    for i in range(0, len(beams)):
        # line to indicate bench item position
        s_canvas['c'].create_line(beams[i].position, 0,
                                  beams[i].position, s_canvas['h'], fill='blue', tag='item')

        for beam in beams[i].beams:
            s_canvas['c'].create_line(beams[i].position,
                                      s_canvas['h']/2 - beam.at(beams[i].position),
                                      beams[i].end_position,
                                      s_canvas['h']/2 - beam.at(beams[i].end_position),
                                      fill='green', tag='beam')
    
bench_items = [ThinLens(300, 310), ThinLens(300, 610), Screen(910)]
source_object = Object(200, 10)

beams = generate_beams(source_object, bench_items)

window = Tk()
window.title('Virtual Bench')
window.minsize(1000, 500)

canvas = {'c': Canvas(window, bg='black', height=500, width=1000),
          'w': 1000, 'h': 500}
canvas['c'].grid(column=0, row=0, columnspan=8)
canvas['c'].create_line(0, canvas['h']/2, canvas['w'], canvas['h']/2, fill='red', tag='axis')

def adjust_source_beams(number_source_beams):
    source_object.beam_count = int(number_source_beams)
    global beams
    beams = generate_beams(source_object, bench_items)
    draw_beams(beams, canvas)
source_beams_slider = Scale(window, command=adjust_source_beams, from_=1, to=13, resolution=1,
                            orient=HORIZONTAL, label='Source Beams')
source_beams_slider.set(5)
source_beams_slider.grid(column=0, row=1)

source_height_frame = Frame(window)
source_height_entry = Entry(source_height_frame, width=10)
source_height_entry.grid(column=0, row=0)
def adjust_source_height():
    source_object.height = int(source_height_entry.get())
    global beams
    beams = generate_beams(source_object, bench_items)
    draw_beams(beams, canvas)
Button(source_height_frame, text='Set Source Height',
       command=adjust_source_height).grid(column=0, row=1)
source_height_frame.grid(column=1, row=1)

add_remove_frame = Frame(window)
def add_bench_item():
    add_bench_window = Toplevel()
    add_bench_window.title('Add Bench Item')
    add_bench_window.minsize(width=200, height=10)

    def do_add_bench_item(addable_item):
        add_bench_window.destroy()

        add_bench_item_window = Toplevel()

        parameter_entries = []
        for parameter in signature(addable_item.__init__).parameters:
            if str(parameter) != 'self':
                parameter_frame = Frame(add_bench_item_window)
                parameter_entry = Entry(parameter_frame)
                parameter_entry.grid(column=0, row=0)
                Label(parameter_frame, text=str(parameter)).grid(column=1, row=0)
                parameter_entries.append(parameter_entry)
                parameter_frame.grid()

        def finish_add_bench_item():
            bench_items.append(addable_item(*[float(pe.get()) for pe in parameter_entries]))
            global beams
            beams = generate_beams(source_object, bench_items)
            draw_beams(beams, canvas)
            add_bench_item_window.destroy()

        Button(add_bench_item_window, command=finish_add_bench_item, text='Add Bench Item').grid()

    addable_items = [ThinLens, LinearPolarizer, QuarterWavePlate, Screen]
    for addable_item in addable_items:
        Button(add_bench_window, command=partial(do_add_bench_item, addable_item),
               text='Add ' + addable_item.__name__).grid()
add_item_button = Button(add_remove_frame, command=add_bench_item, text='Add Bench Item')
add_item_button.grid(column=0, row=0)

def remove_bench_item():
    remove_bench_window = Toplevel()
    remove_bench_window.title('Remove Bench Item')
    remove_bench_window.minsize(width=200, height=10)

    def do_remove_bench_item(i):
        del bench_items[i]
        global beams
        beams = generate_beams(source_object, bench_items)
        draw_beams(beams, canvas)
        remove_bench_window.destroy()

    for i in range(0, len(bench_items)):
        Button(remove_bench_window, command=partial(do_remove_bench_item, i),
               text='Remove ' + str(bench_items[i])).grid()
remove_item_button = Button(add_remove_frame, command=remove_bench_item, text='Remove Bench Item')
remove_item_button.grid(column=0, row=1)
add_remove_frame.grid(column=2, row=1)

def list_bench_items():
    list_bench_window = Toplevel()
    list_bench_window.title('Bench Items')
    list_bench_window.minsize(width=200, height=10)

    Label(list_bench_window, text=str(source_object)).grid()
    for item in bench_items:
        Label(list_bench_window, text=str(item)).grid()
list_bench_button = Button(window, command=list_bench_items, text='List Bench Items')
list_bench_button.grid(column=3, row=1)

pointer_position_frame = LabelFrame(window, text='Position')
pointer_x_label = Label(pointer_position_frame)
pointer_x_label.grid(column=0, row=0)
pointer_y_label = Label(pointer_position_frame)
pointer_y_label.grid(column=0, row=1)
pointer_position_frame.grid(column=4, row=1)

intersection_frame = LabelFrame(window, text='Intersection')
intersection_label = Label(intersection_frame)
intersection_label.grid(column=0, row=1)
intersection_frame.grid(column=5, row=1)

intensity_frame = LabelFrame(window, text='Intensity')
intensity_label = Label(intensity_frame)
intensity_label.grid(column=0, row=1)
intensity_frame.grid(column=6, row=1)

polarization_frame = LabelFrame(window, text='Polarization')
polarization_u_label = Label(polarization_frame, text='u: ')
polarization_u_label.grid(column=0, row=0)
polarization_v_label = Label(polarization_frame, text='v: ')
polarization_v_label.grid(column=0, row=1)
polarization_frame.grid(column=7, row=1)

draw_beams(beams, canvas)

is_running = True
while is_running:
    try:
        pointer_xy = get_pointer_canvas_xy(canvas)
        pointer_x_label.config(text='X: ' + str(pointer_xy[0]))
        pointer_y_label.config(text='Y: ' + str(pointer_xy[1]))

        canvas['c'].delete('xbeam')
        intersection_label.config(text='None')
        polarization_u_label.config(text='u: None')
        polarization_v_label.config(text='v: None')

        if (pointer_xy[1] > -canvas['h']/2 and pointer_xy[1] < canvas['h']/2
            and pointer_xy[0] > 0 and pointer_xy[0] < canvas['w']):
            objects = [source_object] + sorted(bench_items)
            objects_before = [b for b in objects if b.position < pointer_xy[0]]
            objects_after = [b for b in objects if b.position > pointer_xy[0]]

            if len(objects_before) > 1:
                position_before = objects_before[-1].position
            elif len(objects_before) > 0:
                position_before = objects_before[0].position
            else:
                position_before = 0

            if len(objects_after) > 0:
                position_after = objects_after[0].position
            else:
                position_after = canvas['w']

            selected_beam_sets = [b for b in beams if b.position == position_before
                                  and b.end_position == position_after]

            if len(selected_beam_sets) > 0:
                for beam in selected_beam_sets[0].beams:
                    canvas['c'].create_line(0, canvas['h']/2 - beam.at(0),
                                            canvas['w'], canvas['h']/2 - beam.at(canvas['w']),
                                            fill='green', tag='xbeam', dash=(3, 5))

                intersection_label.config(text=str(get_intersection(selected_beam_sets[0])))

                center_beam = sorted(selected_beam_sets[0].beams, key=lambda x: abs(x.ray.t))[0]
                polarization_u_label.config(text='u: {0.real:.2f} + {0.imag:.2f}i'
                                            .format(center_beam.polarization.u))
                polarization_v_label.config(text='v: {0.real:.2f} + {0.imag:.2f}i'
                                            .format(center_beam.polarization.v))

                intensity_label.config(text='{:.2f}'.format(sqrt(abs(center_beam.polarization.u)
                                                                 + abs(center_beam.polarization.v))))

        window.update_idletasks()
        window.update()
    except TclError:
        is_running = False
